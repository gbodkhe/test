from flask import Flask, render_template,request,redirect,url_for,jsonify
import pymysql
import json
import collections

conn=pymysql.connect(host="localhost",user="root",password="root",db="inventory")##Connection to MYSQL DB
con=conn.cursor()###Database cursor

app = Flask(__name__)

@app.route('/')##Main Route

def main():
	"""Loads the index.html which contains all the links"""
	con.execute("SELECT * from Product")
	data=con.fetchall()
	return render_template('index.html', data = data)

@app.route('/createForm')###INSERT Form Route
def createform():
	"""This Function will call the Insert Form which contains TextFields"""
	
	return render_template('insert.html')

@app.route('/insert',methods=['POST','GET'])###Values insert function

def insert():
	"""This function will Gets the values from the FORM and Stored in the DataBase table"""
	if request.method=='POST':
		item_name=request.form['item_name']##Getting values from the FORM
		con.execute("""INSERT into Product(name, quantity, location) VALUES (%s,%s,%s)""",(item_name,request.form['item_quantity'],request.form['store_location']))
		conn.commit()
		con.execute("SELECT * from Product")
		data=con.fetchall()
		return render_template('index.html', data= data)
		# return redirect(url_for('disp',name=item_name))

@app.route('/listItem')###Select Route
def select():
	return render_template('item_list.html')

@app.route('/updateForm')###Update Route
def update():
	"""It will return simple HTML form which contains a textbox to enter id and to update values"""
	return render_template('update.html')

@app.route('/updateid',methods=['POST','GET'])## Gets the values from the Database and throws to Html Form
def update_details():
    if request.method=='POST':
        update_data=request.form['store_location']
        query="SELECT id, name, quantity, location from Product WHERE location=%s"
        param=update_data
        con.execute(query,param)
        data1=con.fetchall()	
        return render_template('update.html',data1=data1)

@app.route('/itemlist',methods=['POST','GET'])## Gets the values from the Database and throws to Html Form
def item_details():
    if request.method=='POST':
        update_data=request.form['store_location']
        query="SELECT name, quantity, location from Product WHERE location=%s"
        param=update_data
        con.execute(query,param)
        data=con.fetchall()	
        return render_template('item_list.html',data=data)

@app.route('/update_details', methods=['POST','GET'])###update values based on ID
def details_update():
	"""It will update the values Based on the id given by the user"""
	if request.method=='POST':
		id=request.form['id']
		print(id)
		name=request.form['name']
		print(name)
		quantity = request.form['quantity']
		location=request.form['location']
		query="UPDATE Product set name=%s,quantity=%s,location=%s where id=%s"
		par=(name,quantity,location,id)
		con.execute(query,par)
		conn.commit()
		# con.execute("SELECT * from Product")
		# data = con.fetchall()
		return render_template('response.html')
